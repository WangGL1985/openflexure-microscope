# Raspberry Pi Camera Lens Removal Tool

The [Raspberry Pi Camera Module v2](../electronics.yml#PiCamera_2) should be supplied with a white plastic tool to unscrew the lens.  If this is missing, there is a [workaround using a printed tool](../../workaround_lens_remover.md).