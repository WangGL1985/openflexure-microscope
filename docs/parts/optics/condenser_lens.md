---
PartData:
  Specs:
    Focal Length: 5 mm
    Outer Diameter: 12.7 mm
    Material: PMMA
  Suppliers:
    AliExpress:
      Link: https://www.aliexpress.com/wholesale?SearchText=13mm+5mm+pmma+lens
      PartNo: Search for '13mm 5mm PMMA lens'
---

# PMMA Condenser Lens

The microscope illumination needs a condenser lens. We use low-cost PMMA lenses designed for collimating LEDs. The do not produce a high quality image, but they are adequate for the illumination. 

These are easiest to buy in bulk from [AliExpress], where they typically cost about £0.20 per lens in lots of 20-100. Unfortunately AliExpress is a marketplace rather than a single supplier, so it is not possible to include a persistent link. We typically [search][AliExpress] for "13mm 5mm PMMA lens". The important things to look for are an outer diameter of between 12.5mm and 13mm and PMMA material.  The focal length is often specified inconsistently: we believe the focal length is between 5mm and 8mm, and the thickness of the lens is around 5mm.  The condenser can be moved up and down, so the microscope should work even if the focal length is not exactly as specified.

[AliExpress]: https://www.aliexpress.com/wholesale?SearchText=13mm+5mm+pmma+lens "Search AliExpress for '13mm 5mm PMMA lens'."